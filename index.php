<?php

require 'vendor/autoload.php';
require 'parameters.php';

$personController = new \App\Controller\PersonController;

if (isset($_GET['route']) && $_GET['route'] == 'list') {
    $personController->findAll();
}

if (isset($_GET['route']) && $_GET['route'] == 'person') {
    $personController->findBy($_GET['id']);
}

if (isset($_GET['route']) && $_GET['route'] == 'create') {
    $personController->create();
}

if (isset($_GET['route']) && $_GET['route'] == 'update') {
    $personController->update();
}

if (isset($_GET['route']) && $_GET['route'] == 'delete') {
    $personController->delete($_GET['id']);
}

$personController->index();
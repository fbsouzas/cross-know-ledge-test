<?php

namespace App;

class View
{
    /**
     * Show a view
     *
     * @link http://laravel.com/docs/4.2/responses#views (function inspired at)
     * @param string $viewName
     * @param array $customVars
     */
    public static function make($viewName, $isJson = false, array $customVars = array())
    {
        extract($customVars);

        $isJson
            ? require_once viewsPath() . 'template.php'
            : require_once viewsPath() . 'templateJson.php';
    }
}

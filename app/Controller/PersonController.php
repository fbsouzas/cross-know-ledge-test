<?php

namespace App\Controller;

use \App\Model\Person;

class PersonController
{
    /**
     * Show index page
     *
     * @return view
     */
    public function index()
    {
        \App\View::make('person.index');
    }

    /**
     * Get all persons
     *
     * @return view
     */
    public function findAll()
    {
    	$persons = Person::findAll();

    	\App\View::make('person.json', false, [
    		'data' => $persons,
    	]);
    }

    /**
     * Get person by id
     *
     * @param integer $id
     * @return view
     */
    public function findBy($id)
    {
        $person = Person::findBy($id);

        \App\View::make('person.json', false, [
            'data' => $person,
        ]);
    }

    /**
     * Create a new person
     *
     * @return view
     */
    public function create()
    {
        \App\View::make('person.json', false, [
            'data' => array(
                'status' => Person::save($_POST['name'], $_POST['lastName'], $_POST['address']),
            ),
        ]);
    }

    /**
     * Update a person
     *
     * @return view
     */
    public function update()
    {
        \App\View::make('person.json', false, [
            'data' => array(
                'status' => Person::update($_POST['id'], $_POST['name'], $_POST['lastName'], $_POST['address']),
            ),
        ]);
    }

    /**
     * Delete a person by id
     *
     * @param integer $id
     * @return view
     */
    public function delete($id)
    {
        \App\View::make('person.json', false, [
            'data' => array(
                'status' => Person::delete($id),
            ),
        ]);
    }
}

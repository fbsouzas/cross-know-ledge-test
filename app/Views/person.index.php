<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Croos Know Legde - Test</title>

    <!-- Bootstrap -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="container">
    <div class="row">
        <div class="col-sm-6">
            <h1>List persons</h1>

            <table class="table">
                <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Last name</th>
                    <th>Address</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="col-sm-6">
            <h1> New and edit person</h1>
            <p class="message"></p>
            <form>
                <div class="form-group">
                    <label for="inputName">Name</label>
                    <input type="text" name="name" id="inputName" class="form-control" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="inputLastName">Last Name</label>
                    <input type="text" name="lastName" id="inputLastName" class="form-control" placeholder="Last Name">
                </div>
                <div class="form-group">
                    <label for="inputAddress">Address</label>
                    <input type="text" name="address" id="inputAddress" class="form-control" placeholder="Address">
                </div>

                <input type="hidden" name="id" class="person-id">

                <a href="javascript:void(null);" class="btn btn-primary btn-create">Create Person</a>
                <a href="javascript:void(null);" class="btn btn-success btn-update">Update Person</a>
                <a href="javascript:void(null);" class="btn btn-warning btn-clean">Clean Form</a>
            </form>

        </div>
    </div>

    <script type="text/javascript">
        var baseUrl = window.location;
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="web/js/app.js"></script>
</body>
</html>
<?php

namespace App\Model;

use App\ConnectionDatabase;

class Person
{
    /**
     * Find all persons in db
     *
     * @return array
     */
    public static function findAll()
    {
        $conn = new ConnectionDatabase;
        $sql = 'SELECT * FROM person ORDER BY name ASC';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Find person by id in db
     *
     * @param integer $id
     * @return array
     */
    public static function findBy($id)
    {
        $conn = new ConnectionDatabase;
        $sql = 'SELECT * FROM person WHERE id = :id ORDER BY name ASC';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * Save a new person in db
     *
     * @param string $name
     * @param string $lastName
     * @param string $address
     * @return boolean
     */
    public static function save($name, $lastName, $address)
    {
        $conn = new ConnectionDatabase;

        if (self::isValid($name, $lastName, $address)) {
            $sql =
                'INSERT INTO
                    person(name, last_name, address)
                VALUES
                    (:name, :lastName, :address)'
            ;

            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':lastName', $lastName);
            $stmt->bindParam(':address', $address);

            return $stmt->execute()
                ? true
                : false;
        }

        return false;
    }

    /**
     * Update person in db
     *
     * @param integer $id
     * @param string  $name
     * @param string  $lastName
     * @param string  $address
     * @return boolean
     */
    public static function update($id, $name, $lastName, $address)
    {
        $conn = new ConnectionDatabase;

        if (self::isValid($name, $lastName, $address)) {
            $sql =
                'UPDATE
                    person
                SET
                    name =:name, last_name = :lastName, address = :address
                WHERE
                    id = :id'
            ;

            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':lastName', $lastName);
            $stmt->bindParam(':address', $address);
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);

            return $stmt->execute()
                ? true
                : false;
        }

        return false;
    }

    /**
     * Delete person by id in db
     *
     * @param integer $id
     * @return boolean
     */
    public static function delete($id)
    {
        $conn = new ConnectionDatabase();
        $sql = 'DELETE FROM person WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);

        return $stmt->execute()
            ? true
            : false;
    }

    /**
     * Checks if fields were filled
     *
     * @param string $name
     * @param string $lastName
     * @param string $address
     * @return boolean
     */
    private static function isValid($name, $lastName, $address)
    {
        return $name && $lastName && $address
            ? true
            : false;
    }
}

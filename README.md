# Test Fábio Souza
### fsouza.me@gmail.com

## Instalation


### DB table for the tests
```sh
CREATE TABLE person(
    id INT UNSIGNED NOT NULL AUTO_INCREMENT, -- id
    name VARCHAR(60) NOT NULL, -- nome
    last_name VARCHAR(80) NOT NULL, -- last_name
    address VARCHAR(80) NOT NULL, -- address
    PRIMARY KEY(id)
) COLLATE=utf8_unicode_ci;
```

## Steps

### 1. Clone the project
```sh
git clone git@bitbucket.org:fbsouzas/cross-know-ledge-test.git
```

### 2. Go to the project directory
```sh
cd /path/to/project/
```

### 3. Copy the parameters file and configure
```sh
cp parameters.php.dist parameters.php
```

### 4. Install the composer
```sh
composer install
```
(If not has **composer** visit [getcomposer.org](http://getcomposer.org) for more details)

### 5. Started php server
```sh
php -S localhost:8000
```

### 6. And visit
```sh
http://localhost:8000
```
jQuery(function ($) {
    'use strict';

    /**
     * get all person and insert in table
     */
    var reloadList = function() {
    	$('tbody').html('');

		$.ajax({
			url: baseUrl.href + '?route=list',
			success: function(data) {
	        	$.each(JSON.parse(data), function(i, person) {
	        		var row ='<tr>'+
					    '<td>'+person.id+'</td>'+
					    '<td>'+person.name+'</td>'+
					    '<td>'+person.last_name+'</td>'+
					    '<td>'+person.address+'</td>'+
					    '<td>'+
					    	'<a href="javascript:void(null);" class="edit-person" data-id="'+person.id+'">Edit</a> | '+
					    	'<a href="javascript:void(null);" class="delete-person" data-id="'+person.id+'">Delete</a>'+
					    '</td>'+
					'</tr>';

	        		$('tbody').append(row);
				});
	    	}
		});
    };

    /**
     * Show message
     */
    var showMessage = function(message) {
    	$('.message').html('').show(800).append(message).delay(5000).hide(800);
    };

    /**
     * Clean list persons
     */
    var cleanList = function() {
    	$('tbody').html('');
    };

    /**
     * Clean form
     */
    var cleanForm = function() {
    	$('input').val('');
    };

    /**
     * Clean form to click button
     */
    !(function() {
    	$(document).on('click', '.btn-clean', function() {
    		cleanForm();
    	});
    })()

    /**
     * Delete a person by id
     */
    !(function() {
    	$(document).on('click', '.delete-person', function() {
    		var $this = $(this);

    		$.ajax({
				url: baseUrl.href + '?route=delete&id=' + $this.data('id'),
				success: function(data) {
					if (JSON.parse(data).status) {
						$this.parents('tr').remove();
					}
		    	}
			});
    	});
    })()

    /**
     * Create a new person
     */
    !(function() {
    	$(document).on('click', '.btn-create', function() {
    		$.ajax({
    			type: 'POST',
    			url: baseUrl.href + '?route=create',
    			data: {
    				name: $('input[name="name"]').val(),
    				lastName: $('input[name="lastName"]').val(),
    				address: $('input[name="address"]').val()
    			},
    			success: function(data) {
    				if (JSON.parse(data).status) {
    					showMessage('Person created!');
    					cleanList();
						reloadList();
						cleanForm();
					} else {
    					showMessage('Something went wrong, try again.');
    					cleanForm();
					}
    			}
    		});
    	});
    })()

    /**
     * Find a person by id
     */
    !(function() {
    	$(document).on('click', '.edit-person', function() {
    		var $this = $(this);

    		$.ajax({
				url: baseUrl.href + '?route=person&id=' + $this.data('id'),
				success: function(data) {
					$('input[name="name"]').val(JSON.parse(data).name);
					$('input[name="lastName"]').val(JSON.parse(data).last_name);
					$('input[name="address"]').val(JSON.parse(data).address);
					$('input[name="id"]').val(JSON.parse(data).id);
		    	}
			});
    	});
    })()

    /**
     * Update a new person
     */
    !(function() {
    	$(document).on('click', '.btn-update', function() {
    		$.ajax({
    			type: 'POST',
    			url: baseUrl.href + '?route=update',
    			data: {
    				id: $('input[name="id"]').val(),
    				name: $('input[name="name"]').val(),
    				lastName: $('input[name="lastName"]').val(),
    				address: $('input[name="address"]').val()
    			},
    			success: function(data) {
    				if (JSON.parse(data).status) {
    					showMessage('Person updated!');
    					cleanList();
						reloadList();
						cleanForm();
					} else {
    					showMessage('Something went wrong, try again.');
    					cleanForm();
					}
    			}
    		});
    	});
    })()

    !(function() {
        reloadList();
    })()
})